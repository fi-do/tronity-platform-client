# coding: utf-8

"""
    Tronity Platform API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0
    Generated by: https://openapi-generator.tech
"""


import pprint
import re  # noqa: F401

import six

from tronity_platform_client.configuration import Configuration


class Idle(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'str',
        'start_time': 'float',
        'start_level': 'float',
        'start_range': 'float',
        'end_time': 'float',
        'end_level': 'float',
        'end_range': 'float',
        'k_wh': 'float',
        'created_at': 'datetime',
        'updated_at': 'datetime',
        'items': 'list[Record]'
    }

    attribute_map = {
        'id': 'id',
        'start_time': 'startTime',
        'start_level': 'startLevel',
        'start_range': 'startRange',
        'end_time': 'endTime',
        'end_level': 'endLevel',
        'end_range': 'endRange',
        'k_wh': 'kWh',
        'created_at': 'createdAt',
        'updated_at': 'updatedAt',
        'items': 'items'
    }

    def __init__(self, id=None, start_time=None, start_level=None, start_range=None, end_time=None, end_level=None, end_range=None, k_wh=None, created_at=None, updated_at=None, items=None, local_vars_configuration=None):  # noqa: E501
        """Idle - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._start_time = None
        self._start_level = None
        self._start_range = None
        self._end_time = None
        self._end_level = None
        self._end_range = None
        self._k_wh = None
        self._created_at = None
        self._updated_at = None
        self._items = None
        self.discriminator = None

        self.id = id
        self.start_time = start_time
        self.start_level = start_level
        self.start_range = start_range
        self.end_time = end_time
        self.end_level = end_level
        self.end_range = end_range
        self.k_wh = k_wh
        self.created_at = created_at
        self.updated_at = updated_at
        self.items = items

    @property
    def id(self):
        """Gets the id of this Idle.  # noqa: E501


        :return: The id of this Idle.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Idle.


        :param id: The id of this Idle.  # noqa: E501
        :type: str
        """
        if self.local_vars_configuration.client_side_validation and id is None:  # noqa: E501
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def start_time(self):
        """Gets the start_time of this Idle.  # noqa: E501


        :return: The start_time of this Idle.  # noqa: E501
        :rtype: float
        """
        return self._start_time

    @start_time.setter
    def start_time(self, start_time):
        """Sets the start_time of this Idle.


        :param start_time: The start_time of this Idle.  # noqa: E501
        :type: float
        """
        if self.local_vars_configuration.client_side_validation and start_time is None:  # noqa: E501
            raise ValueError("Invalid value for `start_time`, must not be `None`")  # noqa: E501

        self._start_time = start_time

    @property
    def start_level(self):
        """Gets the start_level of this Idle.  # noqa: E501


        :return: The start_level of this Idle.  # noqa: E501
        :rtype: float
        """
        return self._start_level

    @start_level.setter
    def start_level(self, start_level):
        """Sets the start_level of this Idle.


        :param start_level: The start_level of this Idle.  # noqa: E501
        :type: float
        """
        if self.local_vars_configuration.client_side_validation and start_level is None:  # noqa: E501
            raise ValueError("Invalid value for `start_level`, must not be `None`")  # noqa: E501

        self._start_level = start_level

    @property
    def start_range(self):
        """Gets the start_range of this Idle.  # noqa: E501


        :return: The start_range of this Idle.  # noqa: E501
        :rtype: float
        """
        return self._start_range

    @start_range.setter
    def start_range(self, start_range):
        """Sets the start_range of this Idle.


        :param start_range: The start_range of this Idle.  # noqa: E501
        :type: float
        """
        if self.local_vars_configuration.client_side_validation and start_range is None:  # noqa: E501
            raise ValueError("Invalid value for `start_range`, must not be `None`")  # noqa: E501

        self._start_range = start_range

    @property
    def end_time(self):
        """Gets the end_time of this Idle.  # noqa: E501


        :return: The end_time of this Idle.  # noqa: E501
        :rtype: float
        """
        return self._end_time

    @end_time.setter
    def end_time(self, end_time):
        """Sets the end_time of this Idle.


        :param end_time: The end_time of this Idle.  # noqa: E501
        :type: float
        """
        if self.local_vars_configuration.client_side_validation and end_time is None:  # noqa: E501
            raise ValueError("Invalid value for `end_time`, must not be `None`")  # noqa: E501

        self._end_time = end_time

    @property
    def end_level(self):
        """Gets the end_level of this Idle.  # noqa: E501


        :return: The end_level of this Idle.  # noqa: E501
        :rtype: float
        """
        return self._end_level

    @end_level.setter
    def end_level(self, end_level):
        """Sets the end_level of this Idle.


        :param end_level: The end_level of this Idle.  # noqa: E501
        :type: float
        """
        if self.local_vars_configuration.client_side_validation and end_level is None:  # noqa: E501
            raise ValueError("Invalid value for `end_level`, must not be `None`")  # noqa: E501

        self._end_level = end_level

    @property
    def end_range(self):
        """Gets the end_range of this Idle.  # noqa: E501


        :return: The end_range of this Idle.  # noqa: E501
        :rtype: float
        """
        return self._end_range

    @end_range.setter
    def end_range(self, end_range):
        """Sets the end_range of this Idle.


        :param end_range: The end_range of this Idle.  # noqa: E501
        :type: float
        """
        if self.local_vars_configuration.client_side_validation and end_range is None:  # noqa: E501
            raise ValueError("Invalid value for `end_range`, must not be `None`")  # noqa: E501

        self._end_range = end_range

    @property
    def k_wh(self):
        """Gets the k_wh of this Idle.  # noqa: E501


        :return: The k_wh of this Idle.  # noqa: E501
        :rtype: float
        """
        return self._k_wh

    @k_wh.setter
    def k_wh(self, k_wh):
        """Sets the k_wh of this Idle.


        :param k_wh: The k_wh of this Idle.  # noqa: E501
        :type: float
        """
        if self.local_vars_configuration.client_side_validation and k_wh is None:  # noqa: E501
            raise ValueError("Invalid value for `k_wh`, must not be `None`")  # noqa: E501

        self._k_wh = k_wh

    @property
    def created_at(self):
        """Gets the created_at of this Idle.  # noqa: E501


        :return: The created_at of this Idle.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this Idle.


        :param created_at: The created_at of this Idle.  # noqa: E501
        :type: datetime
        """
        if self.local_vars_configuration.client_side_validation and created_at is None:  # noqa: E501
            raise ValueError("Invalid value for `created_at`, must not be `None`")  # noqa: E501

        self._created_at = created_at

    @property
    def updated_at(self):
        """Gets the updated_at of this Idle.  # noqa: E501


        :return: The updated_at of this Idle.  # noqa: E501
        :rtype: datetime
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this Idle.


        :param updated_at: The updated_at of this Idle.  # noqa: E501
        :type: datetime
        """
        if self.local_vars_configuration.client_side_validation and updated_at is None:  # noqa: E501
            raise ValueError("Invalid value for `updated_at`, must not be `None`")  # noqa: E501

        self._updated_at = updated_at

    @property
    def items(self):
        """Gets the items of this Idle.  # noqa: E501


        :return: The items of this Idle.  # noqa: E501
        :rtype: list[Record]
        """
        return self._items

    @items.setter
    def items(self, items):
        """Sets the items of this Idle.


        :param items: The items of this Idle.  # noqa: E501
        :type: list[Record]
        """
        if self.local_vars_configuration.client_side_validation and items is None:  # noqa: E501
            raise ValueError("Invalid value for `items`, must not be `None`")  # noqa: E501

        self._items = items

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Idle):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Idle):
            return True

        return self.to_dict() != other.to_dict()
