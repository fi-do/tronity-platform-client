# InlineResponse2008

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**country** | **str** |  | [optional] 
**biomass** | **int** |  | [optional] 
**coal** | **int** |  | [optional] 
**gas** | **int** |  | [optional] 
**geothermal** | **int** |  | [optional] 
**hydro** | **int** |  | [optional] 
**nuclear** | **int** |  | [optional] 
**oil** | **int** |  | [optional] 
**solar** | **int** |  | [optional] 
**wind** | **int** |  | [optional] 
**unknown** | **int** |  | [optional] 
**co2** | **int** |  | [optional] 
**renewable** | **int** |  | [optional] 
**created_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


