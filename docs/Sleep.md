# Sleep

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**start_time** | **float** |  | 
**start_level** | **float** |  | 
**start_range** | **float** |  | 
**end_time** | **float** |  | 
**end_level** | **float** |  | 
**end_range** | **float** |  | 
**k_wh** | **float** |  | 
**created_at** | **datetime** |  | 
**updated_at** | **datetime** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


