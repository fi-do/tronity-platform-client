# Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**name** | **str** |  | 
**country** | **str** |  | 
**url** | **str** |  | 
**category** | **str** |  | 
**currency** | **str** |  | 
**operators** | **list[str]** |  | 
**flat** | **float** |  | 
**base** | **float** |  | 
**prices** | **list[str]** |  | 
**created_at** | **datetime** |  | 
**updated_at** | **datetime** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


