# tronity_platform_client.VehiclesApi

All URIs are relative to *https://api.eu.tronity.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_one_base_vehicle_controller_vehicle**](VehiclesApi.md#delete_one_base_vehicle_controller_vehicle) | **DELETE** /v1/vehicles/{id} | Delete one Vehicle
[**get_many_base_charge_controller_charge**](VehiclesApi.md#get_many_base_charge_controller_charge) | **GET** /v1/vehicles/{vehicleId}/charges | Retrieve many Charge
[**get_many_base_idle_controller_idle**](VehiclesApi.md#get_many_base_idle_controller_idle) | **GET** /v1/vehicles/{vehicleId}/idles | Retrieve many Idle
[**get_many_base_record_controller_record**](VehiclesApi.md#get_many_base_record_controller_record) | **GET** /v1/vehicles/{vehicleId}/records | Retrieve many Record
[**get_many_base_sleep_controller_sleep**](VehiclesApi.md#get_many_base_sleep_controller_sleep) | **GET** /v1/vehicles/{vehicleId}/sleeps | Retrieve many Sleep
[**get_many_base_trip_controller_trip**](VehiclesApi.md#get_many_base_trip_controller_trip) | **GET** /v1/vehicles/{vehicleId}/trips | Retrieve many Trip
[**get_many_base_vehicle_controller_vehicle**](VehiclesApi.md#get_many_base_vehicle_controller_vehicle) | **GET** /v1/vehicles | Retrieve many Vehicle
[**trip_controller_avg**](VehiclesApi.md#trip_controller_avg) | **GET** /v1/vehicles/{vehicleId}/trips/{year}/{month}/avg | Get avg consumption and distance for the month
[**vehicle_controller_battery**](VehiclesApi.md#vehicle_controller_battery) | **GET** /v1/vehicles/{vehicleId}/battery | Read battery&#39;s state of charge
[**vehicle_controller_bulk**](VehiclesApi.md#vehicle_controller_bulk) | **GET** /v1/vehicles/{vehicleId}/bulk | Read bulk data based on vehilce scope
[**vehicle_controller_charge**](VehiclesApi.md#vehicle_controller_charge) | **GET** /v1/vehicles/{vehicleId}/charge | Know whether vehicle is charging
[**vehicle_controller_charge_start**](VehiclesApi.md#vehicle_controller_charge_start) | **POST** /v1/vehicles/{vehicleId}/charge_start | Start Charging
[**vehicle_controller_charge_stop**](VehiclesApi.md#vehicle_controller_charge_stop) | **POST** /v1/vehicles/{vehicleId}/charge_stop | Stop Charging
[**vehicle_controller_location**](VehiclesApi.md#vehicle_controller_location) | **GET** /v1/vehicles/{vehicleId}/location | Returns the last known location of the vehicle in geographic coordinates.
[**vehicle_controller_odometer**](VehiclesApi.md#vehicle_controller_odometer) | **GET** /v1/vehicles/{vehicleId}/odometer | Get odometer from the vehicle
[**vehicle_controller_vin**](VehiclesApi.md#vehicle_controller_vin) | **GET** /v1/vehicles/{vehicleId}/vin | Get vin from the vehicle
[**vehicle_controller_wake_up**](VehiclesApi.md#vehicle_controller_wake_up) | **POST** /v1/vehicles/{vehicleId}/wake_up | Wake Up Car


# **delete_one_base_vehicle_controller_vehicle**
> delete_one_base_vehicle_controller_vehicle(id)

Delete one Vehicle

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    id = 'id_example' # str | 

    try:
        # Delete one Vehicle
        api_instance.delete_one_base_vehicle_controller_vehicle(id)
    except ApiException as e:
        print("Exception when calling VehiclesApi->delete_one_base_vehicle_controller_vehicle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Delete one base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_charge_controller_charge**
> GetManyChargeResponseDto get_many_base_charge_controller_charge(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Charge

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | 
fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Charge
        api_response = api_instance.get_many_base_charge_controller_charge(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->get_many_base_charge_controller_charge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**|  | 
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**GetManyChargeResponseDto**](GetManyChargeResponseDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get paginated response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_idle_controller_idle**
> GetManyIdleResponseDto get_many_base_idle_controller_idle(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Idle

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | 
fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Idle
        api_response = api_instance.get_many_base_idle_controller_idle(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->get_many_base_idle_controller_idle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**|  | 
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**GetManyIdleResponseDto**](GetManyIdleResponseDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get paginated response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_record_controller_record**
> OneOfGetManyRecordResponseDtoarray get_many_base_record_controller_record(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Record

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | 
fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Record
        api_response = api_instance.get_many_base_record_controller_record(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->get_many_base_record_controller_record: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**|  | 
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**OneOfGetManyRecordResponseDtoarray**](OneOfGetManyRecordResponseDtoarray.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get many base response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_sleep_controller_sleep**
> GetManySleepResponseDto get_many_base_sleep_controller_sleep(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Sleep

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | 
fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Sleep
        api_response = api_instance.get_many_base_sleep_controller_sleep(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->get_many_base_sleep_controller_sleep: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**|  | 
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**GetManySleepResponseDto**](GetManySleepResponseDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get paginated response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_trip_controller_trip**
> GetManyTripResponseDto get_many_base_trip_controller_trip(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Trip

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | 
fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Trip
        api_response = api_instance.get_many_base_trip_controller_trip(vehicle_id, fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->get_many_base_trip_controller_trip: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**|  | 
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**GetManyTripResponseDto**](GetManyTripResponseDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get paginated response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_many_base_vehicle_controller_vehicle**
> GetManyVehicleResponseDto get_many_base_vehicle_controller_vehicle(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)

Retrieve many Vehicle

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    fields = ['field1,field2'] # list[str] |  (optional)
s = '{\"name\": \"Michael\"}' # str |  (optional)
sort = ['name,ASC&sort=id,DESC'] # list[str] |  (optional)
join = ['relation||field1,field2'] # list[str] |  (optional)
limit = 56 # int |  (optional)
offset = 56 # int |  (optional)

    try:
        # Retrieve many Vehicle
        api_response = api_instance.get_many_base_vehicle_controller_vehicle(fields=fields, s=s, sort=sort, join=join, limit=limit, offset=offset)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->get_many_base_vehicle_controller_vehicle: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | [**list[str]**](str.md)|  | [optional] 
 **s** | **str**|  | [optional] 
 **sort** | [**list[str]**](str.md)|  | [optional] 
 **join** | [**list[str]**](str.md)|  | [optional] 
 **limit** | **int**|  | [optional] 
 **offset** | **int**|  | [optional] 

### Return type

[**GetManyVehicleResponseDto**](GetManyVehicleResponseDto.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Get paginated response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **trip_controller_avg**
> InlineResponse2007 trip_controller_avg(month, year, vehicle_id, unit_system=unit_system)

Get avg consumption and distance for the month

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    month = 3.4 # float | 
year = 3.4 # float | 
vehicle_id = 'vehicle_id_example' # str | The vehicle id
unit_system = 'unit_system_example' # str | Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. (optional)

    try:
        # Get avg consumption and distance for the month
        api_response = api_instance.trip_controller_avg(month, year, vehicle_id, unit_system=unit_system)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->trip_controller_avg: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **month** | **float**|  | 
 **year** | **float**|  | 
 **vehicle_id** | **str**| The vehicle id | 
 **unit_system** | **str**| Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_battery**
> InlineResponse2003 vehicle_controller_battery(vehicle_id, unit_system=unit_system)

Read battery's state of charge

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id
unit_system = 'unit_system_example' # str | Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. (optional)

    try:
        # Read battery's state of charge
        api_response = api_instance.vehicle_controller_battery(vehicle_id, unit_system=unit_system)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_battery: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 
 **unit_system** | **str**| Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | The credentials are not valid |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**409** | The vehicle may be unreachable. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_bulk**
> InlineResponse2006 vehicle_controller_bulk(vehicle_id, unit_system=unit_system)

Read bulk data based on vehilce scope

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id
unit_system = 'unit_system_example' # str | Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. (optional)

    try:
        # Read bulk data based on vehilce scope
        api_response = api_instance.vehicle_controller_bulk(vehicle_id, unit_system=unit_system)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_bulk: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 
 **unit_system** | **str**| Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | The credentials are not valid |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**409** | The vehicle may be unreachable. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_charge**
> InlineResponse2004 vehicle_controller_charge(vehicle_id, unit_system=unit_system)

Know whether vehicle is charging

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id
unit_system = 'unit_system_example' # str | Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. (optional)

    try:
        # Know whether vehicle is charging
        api_response = api_instance.vehicle_controller_charge(vehicle_id, unit_system=unit_system)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_charge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 
 **unit_system** | **str**| Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | The credentials are not valid |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**409** | The vehicle may be unreachable. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_charge_start**
> vehicle_controller_charge_start(vehicle_id)

Start Charging

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id

    try:
        # Start Charging
        api_instance.vehicle_controller_charge_start(vehicle_id)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_charge_start: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | The credentials are not valid |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**405** | Is not supported by the vehicle. |  -  |
**409** | The vehicle may be unreachable. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_charge_stop**
> vehicle_controller_charge_stop(vehicle_id)

Stop Charging

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id

    try:
        # Stop Charging
        api_instance.vehicle_controller_charge_stop(vehicle_id)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_charge_stop: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | The credentials are not valid |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**405** | Is not supported by the vehicle. |  -  |
**409** | The vehicle may be unreachable. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_location**
> InlineResponse2005 vehicle_controller_location(vehicle_id)

Returns the last known location of the vehicle in geographic coordinates.

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id

    try:
        # Returns the last known location of the vehicle in geographic coordinates.
        api_response = api_instance.vehicle_controller_location(vehicle_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_location: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | The credentials are not valid |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**409** | The vehicle may be unreachable. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_odometer**
> InlineResponse2002 vehicle_controller_odometer(vehicle_id, unit_system=unit_system)

Get odometer from the vehicle

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id
unit_system = 'unit_system_example' # str | Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. (optional)

    try:
        # Get odometer from the vehicle
        api_response = api_instance.vehicle_controller_odometer(vehicle_id, unit_system=unit_system)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_odometer: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 
 **unit_system** | **str**| Tronity Platform supports both Metric and Imperial unit systems of measurement. Set the Unit-System header to metric or imperial to configure your preferred unit system. If no header is specified, defaults to use the Metric system. | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | The credentials are not valid |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**409** | The vehicle may be unreachable. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_vin**
> InlineResponse2001 vehicle_controller_vin(vehicle_id)

Get vin from the vehicle

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id

    try:
        # Get vin from the vehicle
        api_response = api_instance.vehicle_controller_vin(vehicle_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_vin: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vehicle_controller_wake_up**
> vehicle_controller_wake_up(vehicle_id)

Wake Up Car

This is only available for Tesla

### Example

* Bearer (JWT) Authentication (bearer):
```python
from __future__ import print_function
import time
import tronity_platform_client
from tronity_platform_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://api.eu.tronity.io
# See configuration.py for a list of all supported configuration parameters.
configuration = tronity_platform_client.Configuration(
    host = "https://api.eu.tronity.io"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization (JWT): bearer
configuration = tronity_platform_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with tronity_platform_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = tronity_platform_client.VehiclesApi(api_client)
    vehicle_id = 'vehicle_id_example' # str | The vehicle id

    try:
        # Wake Up Car
        api_instance.vehicle_controller_wake_up(vehicle_id)
    except ApiException as e:
        print("Exception when calling VehiclesApi->vehicle_controller_wake_up: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **str**| The vehicle id | 

### Return type

void (empty response body)

### Authorization

[bearer](../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | The credentials are not valid |  -  |
**401** | The provided token were incorrect. |  -  |
**403** | Insufficient permissions to access the requested resource. |  -  |
**405** | Is not supported by the vehicle. |  -  |
**409** | The vehicle may be unreachable. |  -  |
**500** | The server experienced an unexpected error. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

