# GetManyUserResponseDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[User]**](User.md) |  | 
**count** | **float** |  | 
**total** | **float** |  | 
**page** | **float** |  | 
**page_count** | **float** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


