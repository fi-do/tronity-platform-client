# GetManySleepResponseDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**list[Sleep]**](Sleep.md) |  | 
**count** | **float** |  | 
**total** | **float** |  | 
**page** | **float** |  | 
**page_count** | **float** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


