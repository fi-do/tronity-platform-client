# coding: utf-8

"""
    Tronity Platform API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import tronity_platform_client
from tronity_platform_client.api.platform_api import PlatformApi  # noqa: E501
from tronity_platform_client.rest import ApiException


class TestPlatformApi(unittest.TestCase):
    """PlatformApi unit test stubs"""

    def setUp(self):
        self.api = tronity_platform_client.api.platform_api.PlatformApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_carbon_controller_avg(self):
        """Test case for carbon_controller_avg

        The avg co2 polution for country in timeframe  # noqa: E501
        """
        pass

    def test_carbon_controller_latest(self):
        """Test case for carbon_controller_latest

        The latest co2 polution for country and production MW  # noqa: E501
        """
        pass

    def test_create_one_base_app_controller_app(self):
        """Test case for create_one_base_app_controller_app

        Create one App  # noqa: E501
        """
        pass

    def test_create_one_base_webhook_controller_webhook(self):
        """Test case for create_one_base_webhook_controller_webhook

        Create one Webhook  # noqa: E501
        """
        pass

    def test_delete_one_base_app_controller_app(self):
        """Test case for delete_one_base_app_controller_app

        Delete one App  # noqa: E501
        """
        pass

    def test_delete_one_base_webhook_controller_webhook(self):
        """Test case for delete_one_base_webhook_controller_webhook

        Delete one Webhook  # noqa: E501
        """
        pass

    def test_get_many_base_app_controller_app(self):
        """Test case for get_many_base_app_controller_app

        Retrieve many Apps  # noqa: E501
        """
        pass

    def test_get_many_base_user_controller_user(self):
        """Test case for get_many_base_user_controller_user

        Retrieve many User  # noqa: E501
        """
        pass

    def test_get_many_base_webhook_controller_webhook(self):
        """Test case for get_many_base_webhook_controller_webhook

        Retrieve many Webhook  # noqa: E501
        """
        pass

    def test_get_one_base_app_controller_app(self):
        """Test case for get_one_base_app_controller_app

        Retrieve one App  # noqa: E501
        """
        pass

    def test_update_one_base_app_controller_app(self):
        """Test case for update_one_base_app_controller_app

        Update one App  # noqa: E501
        """
        pass

    def test_update_one_base_webhook_controller_webhook(self):
        """Test case for update_one_base_webhook_controller_webhook

        Update one Webhook  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
