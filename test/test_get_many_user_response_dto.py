# coding: utf-8

"""
    Tronity Platform API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import tronity_platform_client
from tronity_platform_client.models.get_many_user_response_dto import GetManyUserResponseDto  # noqa: E501
from tronity_platform_client.rest import ApiException

class TestGetManyUserResponseDto(unittest.TestCase):
    """GetManyUserResponseDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test GetManyUserResponseDto
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # model = tronity_platform_client.models.get_many_user_response_dto.GetManyUserResponseDto()  # noqa: E501
        if include_optional :
            return GetManyUserResponseDto(
                data = [
                    tronity_platform_client.models.user.User(
                        id = '0', 
                        company = '0', 
                        first_name = '0', 
                        last_name = '0', 
                        street = '0', 
                        zip_code = '0', 
                        city = '0', 
                        country = '0', 
                        email = '0', 
                        password = '0', 
                        role = 'admin', 
                        scopes = 'read_battery', 
                        package = 1.337, 
                        created_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                        updated_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), )
                    ], 
                count = 1.337, 
                total = 1.337, 
                page = 1.337, 
                page_count = 1.337
            )
        else :
            return GetManyUserResponseDto(
                data = [
                    tronity_platform_client.models.user.User(
                        id = '0', 
                        company = '0', 
                        first_name = '0', 
                        last_name = '0', 
                        street = '0', 
                        zip_code = '0', 
                        city = '0', 
                        country = '0', 
                        email = '0', 
                        password = '0', 
                        role = 'admin', 
                        scopes = 'read_battery', 
                        package = 1.337, 
                        created_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                        updated_at = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), )
                    ],
                count = 1.337,
                total = 1.337,
                page = 1.337,
                page_count = 1.337,
        )

    def testGetManyUserResponseDto(self):
        """Test GetManyUserResponseDto"""
        inst_req_only = self.make_instance(include_optional=False)
        inst_req_and_optional = self.make_instance(include_optional=True)


if __name__ == '__main__':
    unittest.main()
